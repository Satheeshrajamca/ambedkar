package com.android.ambedkar.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.activity.EventDetailsActivity;
import com.android.ambedkar.adapter.EventAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.model.EventList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;
import static com.android.ambedkar.utility.Utill.EVENTPATH;
import static com.android.ambedkar.utility.Utill.VIDEO125PATH;

/**
 * Created by satheeshraja on 4/23/17.
 */

public class DepartmentFunctionsFragment extends Fragment {
    View rootView;
    Unbinder unbinder;
    RequestQueue requestQueue;
    @BindView(R.id.listviewEvents)
    ListView listviewEvents;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    SharedPreferences sharedpreferences;
    ThinDownloadManager downloadManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_events, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            requestQueue = VolleyRequestQueue.getInstance(getActivity().getApplicationContext()).getRequestQueue();
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (Utill.isOnline(getActivity()) || sharedpreferences.getString("DepartmentResponse", null) == null)
                sendJsonrequestDepartmentFunctionList();
            else
                parseJson(sharedpreferences.getString("DepartmentResponse", null));

        }
        return rootView;
    }

    private void sendJsonrequestDepartmentFunctionList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlEvents,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("DepartmentResponse", response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("events_list", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    public static List<EventList> listEvents;

    public void parseJson(String response) {
        listEvents = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                downloadManager = new ThinDownloadManager(length);
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    downloadManager(jsonObject2.getString("event_id"), jsonObject2.getString("event_image_url"));
                    EventList albumList = new EventList(jsonObject2.getString("event_id"), jsonObject2.getString("event_title"),
                            jsonObject2.getString("event_desc"), jsonObject2.getString("event_image_url"),
                            jsonObject2.getString("event_date"), jsonObject2.getString("event_venue"));
                    listEvents.add(albumList);
                }

            } else {
                Toast.makeText(getActivity(), jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        EventAdapter albumListAdapter = new EventAdapter(getActivity(), R.layout.row_events, listEvents);
        listviewEvents.setAdapter(albumListAdapter);
        listviewEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), EventDetailsActivity.class)
                        .putExtra("EventPos", "" + (position)));
            }
        });

    }

    HashMap<Integer, String> hashMap = new HashMap<>();

    private void downloadManager(String event_id, String event_image_url) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + EVENTPATH, event_id + ".jpg");
        if (!internalFile.exists()) {
            Uri downloadUri = Uri.parse(event_image_url);
            File file = new File(getActivity().getExternalFilesDir(""), EVENTPATH);
            file.mkdir();
            File file1 = new File(file, event_id + ".jpg");
            Uri destinationUri = Uri.parse(file1.getAbsolutePath());
            Log.i("DestiantionUrl", destinationUri.getPath());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new com.thin.downloadmanager.DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(new DownloadStatusListener() {
                        @Override
                        public void onDownloadComplete(int id) {
                            Log.i("statusfile", "Completed:" + hashMap.get(id));
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            Log.i("statusfile", "Failed");
                            showInternalFilesDir(hashMap.get(id));
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
                        }
                    });
            downloadManager.add(downloadRequest);
            hashMap.put(downloadRequest.getDownloadId(), event_id);
        }
    }

    private void showInternalFilesDir(String video_id) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + VIDEO125PATH, video_id + ".jpg");
        internalFile.delete();
    }
}
