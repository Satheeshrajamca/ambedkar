package com.android.ambedkar.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.ambedkar.activity.GalleryActivity;
import com.android.ambedkar.adapter.AlbumListAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.R;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.internal.Utility;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.android.ambedkar.utility.Utill.ALBUMPATH;

/**
 * Created by satheeshraja on 4/23/17.
 */

public class AlbumFragment extends Fragment {
    View rootView;
    Unbinder unbinder;
    RequestQueue requestQueue;
    @BindView(R.id.gridGallery)
    GridView gridGallery;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    SharedPreferences sharedpreferences;
    public static String MyPREFERENCES = "Offline";
    ThinDownloadManager downloadManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            requestQueue = VolleyRequestQueue.getInstance(getActivity().getApplicationContext()).getRequestQueue();
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (Utill.isOnline(getActivity()) || sharedpreferences.getString("GalleryResponse", null) == null)
                sendJsonrequestAlbumList();
            else
                parseJson(sharedpreferences.getString("GalleryResponse", null));
        }
        return rootView;
    }

    private void sendJsonrequestAlbumList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlGettingAlbum,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("GalleryResponse", response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("album_list", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private List<AlbumList> listAlbum;

    public void parseJson(String response) {
        listAlbum = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                downloadManager = new ThinDownloadManager(length);
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    downloadManager(jsonObject2.getString("album_id") + ".jpg", jsonObject2.getString("abum_cover_photo"));
                    AlbumList albumList = new AlbumList(jsonObject2.getString("album_id"), jsonObject2.getString("album_name"),
                            jsonObject2.getString("abum_cover_photo"), jsonObject2.getString("album_photos_count"));
                    listAlbum.add(albumList);
                }

            } else {
                Toast.makeText(getActivity(), jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        AlbumListAdapter albumListAdapter = new AlbumListAdapter(getActivity(), R.layout.row_album, listAlbum);
        gridGallery.setAdapter(albumListAdapter);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!Utill.isOnline(getActivity()) && sharedpreferences.getString(listAlbum.get(position).getAlbum_id(), null) == null) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Internet Issue!")
                            .content("Please check your internet connection")
                            .positiveText("Goto")
                            .negativeText("Cancel")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                    startActivity(intent);
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {
                    startActivity(new Intent(getActivity(), GalleryActivity.class).putExtra("AlbumId", listAlbum.get(position).getAlbum_id())
                            .putExtra("GalleryPos", "" + (position + 1)));
                }
            }
        });

    }

    HashMap<Integer, String> hashMap = new HashMap<>();

    private void downloadManager(String album_id, String abum_cover_photo) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + ALBUMPATH, album_id);
        if (!internalFile.exists()) {
            Uri downloadUri = Uri.parse(abum_cover_photo);
            File file = new File(getActivity().getExternalFilesDir(""), ALBUMPATH);
            file.mkdir();
            File file1 = new File(file, album_id);
            Uri destinationUri = Uri.parse(file1.getAbsolutePath());
            Log.i("DestiantionUrl", destinationUri.getPath());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new com.thin.downloadmanager.DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setStatusListener(new DownloadStatusListenerV1() {
                        @Override
                        public void onDownloadComplete(DownloadRequest downloadRequest) {
                            Log.i("statusfile", "Completed:" + hashMap.get(downloadRequest.getDownloadId()));
                        }

                        @Override
                        public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                            Log.i("statusfile", "Failed");
                            showInternalFilesDir(hashMap.get(downloadRequest.getDownloadId()));
                        }

                        @Override
                        public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

                        }
                    });
            downloadManager.add(downloadRequest);
            hashMap.put(downloadRequest.getDownloadId(), album_id);
        }
    }

    private void showInternalFilesDir(String album_id) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + ALBUMPATH, album_id);
        if (internalFile.isFile())
            internalFile.delete();

    }
}
