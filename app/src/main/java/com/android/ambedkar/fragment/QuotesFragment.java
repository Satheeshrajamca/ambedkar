package com.android.ambedkar.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.adapter.QuoteListAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.model.QuoteList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;

/**
 * Created by satheeshr on 27-04-2017.
 */

public class QuotesFragment extends android.support.v4.app.Fragment {
    View rootView;
    Unbinder unbinder;
    RequestQueue requestQueue;
    @BindView(R.id.listviewEvents)
    ListView listviewQuotes;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    SharedPreferences sharedpreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_events, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            requestQueue = VolleyRequestQueue.getInstance(getActivity().getApplicationContext()).getRequestQueue();
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (Utill.isOnline(getActivity()) || sharedpreferences.getString("QuotesResponse", null) == null)
                sendJsonrequestQuoteList();
            else
                parseJson(sharedpreferences.getString("QuotesResponse", null));

        }
        return rootView;
    }

    private void sendJsonrequestQuoteList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlQuotes,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("QuotesResponse", response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("quote_list", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    public static List<QuoteList> listQuotes;

    public void parseJson(String response) {
        listQuotes = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    QuoteList quoteList = new QuoteList(jsonObject2.getString("quote_id"), jsonObject2.getString("quote"));
                    listQuotes.add(quoteList);
                }

            } else {
                Toast.makeText(getActivity(), jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        QuoteListAdapter albumListAdapter = new QuoteListAdapter(getActivity(), R.layout.row_quotes, listQuotes);
        listviewQuotes.setAdapter(albumListAdapter);
    }
}
