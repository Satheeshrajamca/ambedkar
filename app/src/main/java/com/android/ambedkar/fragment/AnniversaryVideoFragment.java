package com.android.ambedkar.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.activity.VideoPlayActivity;
import com.android.ambedkar.adapter.VideoListAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.model.VideoList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.okhttp.internal.Util;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;
import static com.android.ambedkar.utility.Utill.ALBUM125PATH;
import static com.android.ambedkar.utility.Utill.VIDEO125PATH;
import static com.android.ambedkar.utility.Utill.VIDEOPATH;

/**
 * Created by satheeshraja on 7/12/17.
 */

public class AnniversaryVideoFragment extends android.support.v4.app.Fragment implements VideoListAdapter.VideoDescription {
    View rootView;
    Unbinder unbinder;
    RequestQueue requestQueue;
    @BindView(R.id.gridGallery)
    GridView gridGallery;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    SharedPreferences sharedpreferences;
    ThinDownloadManager downloadManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            requestQueue = VolleyRequestQueue.getInstance(getActivity().getApplicationContext()).getRequestQueue();
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (Utill.isOnline(getActivity()) || sharedpreferences.getString("125VideosResponse", null) == null)
                sendJsonrequest125VideoList();
            else
                parseJson(sharedpreferences.getString("125VideosResponse", null));

        }
        return rootView;
    }

    private void sendJsonrequest125VideoList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlGettingVideo,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("125VideosResponse", response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("video_list", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    public static List<VideoList> listVideos;

    public void parseJson(String response) {
        listVideos = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                downloadManager = new ThinDownloadManager(length);
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    downloadManager(jsonObject2.getString("video_id"), jsonObject2.getString("video_image_url"));
                    VideoList albumList = new VideoList(jsonObject2.getString("video_id"), jsonObject2.getString("video_title"),
                            jsonObject2.getString("video_image_url"), jsonObject2.getString("video_url"),jsonObject2.getString("video_description"));
                    listVideos.add(albumList);
                }

            } else {
                Toast.makeText(getActivity(), jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        VideoListAdapter albumListAdapter = new VideoListAdapter(AnniversaryVideoFragment.this,getActivity(), R.layout.row_video, listVideos,VIDEO125PATH);
        gridGallery.setAdapter(albumListAdapter);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Utill.isOnline(getActivity()))
                    startActivity(new Intent(getActivity(), VideoPlayActivity.class).putExtra("VideoUrl", listVideos.get(position).getVideo_url()));
                else
                    Utill.showNetworkDialog(getActivity());
            }
        });

    }

    HashMap<Integer, String> hashMap = new HashMap<>();

    private void downloadManager(String video_id, String video_image_url) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + VIDEO125PATH, video_id + ".jpg");
        if (!internalFile.exists()) {
            Uri downloadUri = Uri.parse(video_image_url);
            File file = new File(getActivity().getExternalFilesDir(""), VIDEO125PATH);
            file.mkdir();
            File file1 = new File(file, video_id + ".jpg");
            Uri destinationUri = Uri.parse(file1.getAbsolutePath());
            Log.i("DestiantionUrl", destinationUri.getPath());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new com.thin.downloadmanager.DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(new DownloadStatusListener() {
                        @Override
                        public void onDownloadComplete(int id) {
                            Log.i("statusfile", "Completed:" + hashMap.get(id));
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            Log.i("statusfile", "Failed");
                            showInternalFilesDir(hashMap.get(id));
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
                        }
                    });
            downloadManager.add(downloadRequest);
            hashMap.put(downloadRequest.getDownloadId(), video_id);
        }
    }

    private void showInternalFilesDir(String video_id) {
        File internalFile = new File(getActivity().getExternalFilesDir("") + VIDEO125PATH, video_id + ".jpg");
        internalFile.delete();
    }

    @Override
    public void showVideoDescription(int position) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_videodes, null);
        TextView textVideoDesc = (TextView) view.findViewById(R.id.textVideoDesc);
        TextView textVideoTile = (TextView) view.findViewById(R.id.textVideoTile);
        textVideoTile.setText(listVideos.get(position).getVideo_title());
        ImageView imageClose = (ImageView) view.findViewById(R.id.closeBottomSheet);
        textVideoDesc.setText(listVideos.get(position).getVideo_desc());
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
