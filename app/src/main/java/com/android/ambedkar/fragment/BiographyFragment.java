package com.android.ambedkar.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.GridView;

import com.android.ambedkar.R;
import com.android.ambedkar.common.VolleyRequestQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by satheeshraja on 4/23/17.
 */

public class BiographyFragment extends Fragment {
    View rootView;
    Unbinder unbinder;
    @BindView(R.id.webview)
    WebView webView;
    ProgressDialog pDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_biography, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            init();
            listener();
        }
        return rootView;
    }

    private void init() {
        webView.getSettings().setJavaScriptEnabled(true);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Biography");
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        if (sharedpreferences.getString("Locale", "en").equals("en")) {
            webView.loadUrl("file:///android_asset/Events/Babasaheb.html");
        } else {
            webView.loadUrl("file:///android_asset/Events/Babasahebtamil.html");
        }
    }

    private void listener() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
            }
        });
    }
}
