package com.android.ambedkar.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.adapter.BookListAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.model.BookList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;
import static com.facebook.login.widget.ProfilePictureView.TAG;

/**
 * Created by satheeshraja on 4/23/17.
 */

public class BooksFragment extends Fragment {
    View rootView;
    Unbinder unbinder;
    RequestQueue requestQueue;
    @BindView(R.id.gridGallery)
    GridView gridGallery;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    ProgressDialog pDialog;
    SharedPreferences sharedpreferences;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Downloading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
            unbinder = ButterKnife.bind(this, rootView);
            requestQueue = VolleyRequestQueue.getInstance(getActivity().getApplicationContext()).getRequestQueue();
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            if (Utill.isOnline(getActivity()) || sharedpreferences.getString("BookResponse", null) == null)
                sendJsonrequestBookList();
            else
                parseJson(sharedpreferences.getString("BookResponse", null));

        }
        return rootView;
    }

    private void sendJsonrequestBookList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlGettingBooks,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("BookResponse", response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("book_list", "true");
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private List<BookList> listBooks;

    public void parseJson(String response) {
        listBooks = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    BookList albumList = new BookList(jsonObject2.getString("book_id"), jsonObject2.getString("book_name"),
                            jsonObject2.getString("book_url"), jsonObject2.getString("book_cover_img_url"), jsonObject2.getString("book_desc"));
                    listBooks.add(albumList);
                }

            } else {
                Toast.makeText(getActivity(), jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        BookListAdapter albumListAdapter = new BookListAdapter(getActivity(), R.layout.row_books, listBooks);
        gridGallery.setAdapter(albumListAdapter);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedBookPos = position;
                if (isStoragePermissionGranted()) {
                    /*startActivity(new Intent(getActivity(), BooksActivity.class).putExtra("BookUrl", listBooks.get(position).getBook_url()).
                            putExtra("BookTitle", listBooks.get(position).getBook_name()));*/
                    bookTitle = listBooks.get(selectedBookPos).getBook_name();
                    bookUrl = listBooks.get(selectedBookPos).getBook_url();
                    File pdfFile = new File(getActivity().getExternalFilesDir(null), bookTitle);
                    if (!pdfFile.isFile()) {
                        if (Utill.isOnline(getActivity()))
                            downloadPdfInternal();
                        else
                            Utill.showNetworkDialog(getActivity());
                    } else {
                        loadPdfFile(pdfFile);
                    }
                }
            }
        });

    }

    int selectedBookPos;

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    String bookTitle, bookUrl;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            /*startActivity(new Intent(getActivity(), BooksActivity.class).putExtra("BookUrl", listBooks.get(selectedBookPos).getBook_url()).
                    putExtra("BookTitle", listBooks.get(selectedBookPos).getBook_name()));*/
        }
    }

    File folder;

    private void downloadPdfInternal() {
        if (isExternalStorageWritable()) {
            folder = new File(getActivity().getExternalFilesDir(null), bookTitle);
            try {
                folder.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        OkHttpClient client = new OkHttpClient();
        com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder().url(bookUrl).build();
        pDialog.show();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(com.squareup.okhttp.Request request, IOException e) {
                pDialog.dismiss();
                e.printStackTrace();
            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Failed to download file: " + response);
                }
                FileOutputStream fos = new FileOutputStream(folder);
                fos.write(response.body().bytes());
                fos.close();
                loadPdfFile(folder);
            }
        });
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private void loadPdfFile(File pdfFile) {
        pDialog.dismiss();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }
}
