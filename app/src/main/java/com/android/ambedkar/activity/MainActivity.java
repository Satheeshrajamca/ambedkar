package com.android.ambedkar.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.ambedkar.fragment.AlbumFragment;
import com.android.ambedkar.R;
import com.android.ambedkar.fragment.AnniversaryVideoFragment;
import com.android.ambedkar.fragment.BiographyFragment;
import com.android.ambedkar.fragment.BirthdayCelebrationGalleryFragment;
import com.android.ambedkar.fragment.BooksFragment;
import com.android.ambedkar.fragment.DepartmentFunctionsFragment;
import com.android.ambedkar.fragment.QuotesFragment;
import com.android.ambedkar.fragment.VideosFragment;
import com.android.ambedkar.utility.Utill;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ActionBar actionBar;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.nav_books));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        Fragment fragment = new BooksFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.container, fragment).commit();
        selectedId = R.id.nav_books;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    int selectedId;

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (selectedId != id) {
            Fragment fragment = null;
            if (id == R.id.nav_books) {
                selectedId = id;
                actionBar.setTitle(getResources().getString(R.string.nav_books));
                fragment = new BooksFragment();
            } else if (id == R.id.nav_gallery) {
                if (Utill.isOnline(this) || sharedpreferences.getString("GalleryResponse", null) != null) {
                    selectedId = id;
                    actionBar.setTitle(getResources().getString(R.string.nav_gallery));
                    fragment = new AlbumFragment();
                } else {
                    Utill.showNetworkDialog(this);
                }

            } else if (id == R.id.nav_video) {
                if (Utill.isOnline(this) || sharedpreferences.getString("VideosResponse", null) != null) {
                    selectedId = id;
                    actionBar.setTitle(getResources().getString(R.string.nav_videos));
                    fragment = new VideosFragment();
                } else {
                    Utill.showNetworkDialog(this);
                }
            } else if (id == R.id.nav_international_events) {
                /*actionBar.setTitle(getResources().getString(R.string.nav_videos));
                fragment = new VideosFragment();*/
            } else if (id == R.id.nav_bday_anniversary_videos) {
                if (Utill.isOnline(this) || sharedpreferences.getString("125VideosResponse", null) != null) {
                    selectedId = id;
                    actionBar.setTitle(getResources().getString(R.string.nav_bday_anniversary_video));
                    fragment = new AnniversaryVideoFragment();
                } else {
                    Utill.showNetworkDialog(this);
                }

            } else if (id == R.id.nav_bday_anniversary_images) {
                if (Utill.isOnline(this) || sharedpreferences.getString("125Gallery", null) != null) {
                    selectedId = id;
                    actionBar.setTitle(getResources().getString(R.string.nav_bday_anniversary_images));
                    fragment = new BirthdayCelebrationGalleryFragment();
                } else {
                    Utill.showNetworkDialog(this);
                }

            } else if (id == R.id.nav_biography) {
                selectedId = id;
                fragment = new BiographyFragment();
                actionBar.setTitle(getResources().getString(R.string.nav_biography));
            } else if (id == R.id.nav_influence_baba_sahib) {
                /*fragment = new BiographyFragment();
                actionBar.setTitle(getResources().getString(R.string.nav_biography));*/
            } else if (id == R.id.nav_department_functions) {
                if (Utill.isOnline(this) || sharedpreferences.getString("DepartmentResponse", null) != null) {
                    selectedId = id;
                    fragment = new DepartmentFunctionsFragment();
                    actionBar.setTitle(getResources().getString(R.string.nav_events));
                } else {
                    Utill.showNetworkDialog(this);
                }

            } else if (id == R.id.nav_quotes) {
                if (Utill.isOnline(this) || sharedpreferences.getString("QuotesResponse", null) != null) {
                    selectedId = id;
                    fragment = new QuotesFragment();
                    actionBar.setTitle(getResources().getString(R.string.nav_quotes));
                } else {
                    Utill.showNetworkDialog(this);
                }

            } else if (id == R.id.nav_logout) {
                selectedId = id;
                actionBar.setTitle(getResources().getString(R.string.nav_logout));
                finish();
                SharedPreferences sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
            } else {
                Log.e("MainActivity", "Error in creating fragment");
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
