package com.android.ambedkar.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.adapter.GalleryListAdapter;
import com.android.ambedkar.common.UrlApi;
import com.android.ambedkar.common.VolleyRequestQueue;
import com.android.ambedkar.model.GalleryList;
import com.android.ambedkar.utility.Utill;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;
import static com.android.ambedkar.utility.Utill.ALBUMPATH;

/**
 * Created by satheeshraja on 4/25/17.
 */

public class GalleryActivity extends AppCompatActivity {
    RequestQueue requestQueue;
    @BindView(R.id.gridGallery)
    GridView gridGallery;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SharedPreferences sharedpreferences;
    String albumIdSharedPref;
    ThinDownloadManager downloadManager;
    private String albumId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_gallery);
        ButterKnife.bind(this);
        requestQueue = VolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        albumIdSharedPref = sharedpreferences.getString(getIntent().getStringExtra("AlbumId"), null);
        albumId = getIntent().getStringExtra("AlbumId");
        if (Utill.isOnline(this) || albumIdSharedPref == null)
            sendJsonrequestGalleryList();
        else
            parseJson(albumIdSharedPref);

        //Initialize Toolbar
        toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(getString(R.string.msg_gallery_no) + "\t" + getIntent().getStringExtra("GalleryPos"));
        }
    }

    private void sendJsonrequestGalleryList() {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlApi.urlGettingGallery,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressbar.setVisibility(View.GONE);
                        parseJson(response);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(getIntent().getStringExtra("AlbumId"), response);
                        editor.commit();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressbar.setVisibility(View.GONE);
                        Toast.makeText(GalleryActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("album_id", getIntent().getStringExtra("AlbumId"));
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    public static List<GalleryList> listGallery;

    private void parseJson(String response) {
        listGallery = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("response_status");
            String status = jsonObject1.getString("status");
            if (status.equals("Success")) {
                JSONArray jsonArray = jsonObject.getJSONArray("response_db_values");
                int length = jsonArray.length();
                downloadManager = new ThinDownloadManager(length);
                for (int i = 0; i < length; i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                    downloadManager(jsonObject2.getString("photo_id"), jsonObject2.getString("photo_url"));
                    GalleryList albumList = new GalleryList(jsonObject2.getString("photo_id"), jsonObject2.getString("photo_title"),
                            jsonObject2.getString("photo_desc"), jsonObject2.getString("photo_url"));
                    listGallery.add(albumList);
                }

            } else {
                Toast.makeText(GalleryActivity.this, jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            Toast.makeText(GalleryActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        GalleryListAdapter albumListAdapter = new GalleryListAdapter(GalleryActivity.this, R.layout.row_gallery, listGallery, albumId);
        gridGallery.setAdapter(albumListAdapter);
        gridGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(GalleryActivity.this, GalleryViewPager.class).putExtra("ImagePosition", "" + position).putExtra("From", "Gallery").putExtra("ALBUM_ID", albumId));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    HashMap<Integer, String> hashMap = new HashMap<>();

    private void downloadManager(String photo_id, String abum_cover_photo) {
        File internalFile = new File(getExternalFilesDir("") + ALBUMPATH + albumId, photo_id + ".jpg");
        if (!internalFile.exists()) {
            Uri downloadUri = Uri.parse(abum_cover_photo);
            File file = new File(getExternalFilesDir(""), ALBUMPATH + albumId);
            file.mkdir();
            File file1 = new File(file, photo_id + ".jpg");
            Uri destinationUri = Uri.parse(file1.getAbsolutePath());
            Log.i("DestiantionUrl", destinationUri.getPath());
            DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new com.thin.downloadmanager.DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(new DownloadStatusListener() {
                        @Override
                        public void onDownloadComplete(int id) {
                            Log.i("statusfile", "Completed:" + hashMap.get(id));
                        }

                        @Override
                        public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                            Log.i("statusfile", "Failed");
                            showInternalFilesDir(hashMap.get(id));
                        }

                        @Override
                        public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {
                        }
                    });
            downloadManager.add(downloadRequest);
            hashMap.put(downloadRequest.getDownloadId(), photo_id);
        }
    }

    private void showInternalFilesDir(String photo_id) {
        File internalFile = new File(getExternalFilesDir("") + ALBUMPATH + albumId, photo_id + ".jpg");
        internalFile.delete();
    }
}
