package com.android.ambedkar.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Xml;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.ambedkar.fragment.DepartmentFunctionsFragment.listEvents;
import static com.android.ambedkar.utility.Utill.EVENTPATH;

/**
 * Created by satheeshraja on 4/27/17.
 */

public class EventDetailsActivity extends AppCompatActivity {
    @BindView(R.id.eventTitle)
    TextView eventTitle;
    @BindView(R.id.eventDate)
    TextView eventDate;
    @BindView(R.id.venue)
    TextView venue;
    @BindView(R.id.eventImage)
    ImageView eventImage;
    @BindView(R.id.eventDescription)
    WebView eventDescription;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details);
        ButterKnife.bind(this);
        int position = Integer.parseInt(getIntent().getStringExtra("EventPos"));
        eventTitle.setText(listEvents.get(position).getEvent_title());
        eventDate.setText(listEvents.get(position).getEvent_date());
        venue.setText(listEvents.get(position).getEvent_venue());
        if (Utill.isOnline(this)) {
            Picasso.with(this).load(listEvents.get(position).getEvent_image_url()).into(eventImage);
        } else {
            Picasso.with(this).load(new File(this.getExternalFilesDir("") + EVENTPATH + listEvents.get(position).getEvent_id() + ".jpg")).
                    error(R.drawable.error_drawable).into(eventImage);
        }
        eventDescription.getSettings().setSupportZoom(true);
        eventDescription.getSettings().setDisplayZoomControls(true);
        eventDescription.getSettings().setBuiltInZoomControls(true);
        eventDescription.loadData(URLEncoder.encode(listEvents.get(position).getEvent_desc()).replaceAll("\\+", " "), "text/html", Xml.Encoding.UTF_8.toString());
        //eventDescription.setText(listEvents.get(position).getEvent_desc());
        //Initialize Toolbar
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle(listEvents.get(position).getEvent_title());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
