package com.android.ambedkar.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.android.ambedkar.R;
import com.android.ambedkar.utility.Utill;
import com.microsoft.azure.mobile.MobileCenter;
import com.microsoft.azure.mobile.analytics.Analytics;
import com.microsoft.azure.mobile.crashes.Crashes;
import com.microsoft.azure.mobile.distribute.Distribute;


import java.util.Locale;

import butterknife.OnClick;

import static com.android.ambedkar.fragment.AlbumFragment.MyPREFERENCES;

/**
 * Created by satheeshr on 21-04-2017.
 */

public class LoginActivity extends Activity {
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileCenter.start(getApplication(), "a24fdfe1-da29-4553-9f38-413d677ed4d6",
                Analytics.class, Crashes.class,Distribute.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.getBoolean("ISLOGGEDIN", false)) {
            changeLocale(sharedpreferences.getString("Locale", "en"));
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            setContentView(R.layout.activity_login);
        }
    }

    @OnClick({R.id.btnEnterTamil, R.id.btnEnterEnglish})
    public void submit(View view) {
        if (Utill.isOnline(this) || sharedpreferences.getString("BookResponse", null) != null) {
            if (view.getId() == R.id.btnEnterEnglish) {
                changeLocale("en");
            } else {
                changeLocale("ta");
            }
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Utill.showNetworkDialog(this);
        }
    }

    private void changeLocale(String languageCode) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("ISLOGGEDIN", true);
        editor.putString("Locale", languageCode);
        editor.apply();

        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

}
