package com.android.ambedkar.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.android.ambedkar.R;

import java.util.Locale;

import butterknife.OnClick;

/**
 * Created by satheeshraja on 4/20/17.
 */

public class LanguageSelectorActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selector);
    }

    @OnClick({R.id.english_btn, R.id.tamil_btn})
    public void launcher(View view) {
        if (view.getId() == R.id.english_btn) {
            changeLocale("en");
            startActivity(new Intent(this, LoginActivity.class));

        } else {
            changeLocale("ta");
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    private void changeLocale(String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

}
