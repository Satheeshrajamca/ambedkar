package com.android.ambedkar.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.ambedkar.R;
import com.android.ambedkar.adapter.MyCustomPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.android.ambedkar.activity.GalleryActivity.listGallery;
import static com.android.ambedkar.fragment.BirthdayCelebrationGalleryFragment.listGallery125Celebration;

/**
 * Created by satheeshraja on 4/25/17.
 */

public class GalleryViewPager extends AppCompatActivity {
    ViewPager viewPager;
    MyCustomPagerAdapter myCustomPagerAdapter;
    ProgressBar progressbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private String albumId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_gallery);
        albumId = getIntent().getStringExtra("ALBUM_ID");
        ButterKnife.bind(this);
        //Initialize Toolbar
        toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        if (getIntent().getStringExtra("From").equals("Gallery"))
            myCustomPagerAdapter = new MyCustomPagerAdapter(this, listGallery,albumId);
        else
            myCustomPagerAdapter = new MyCustomPagerAdapter(this, listGallery125Celebration,"");
        viewPager.setAdapter(myCustomPagerAdapter);
        viewPager.setCurrentItem(Integer.parseInt(getIntent().getStringExtra("ImagePosition")));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
