package com.android.ambedkar.utility;

import com.android.volley.toolbox.HurlStack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

/**
 * Created by satheeshr on 05-07-2017.
 */

public class ProxyHurlStack extends HurlStack {

    @Override
    protected HttpURLConnection createConnection(URL url) throws IOException {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.1.17", 8888));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
        return conn;
    }
}
