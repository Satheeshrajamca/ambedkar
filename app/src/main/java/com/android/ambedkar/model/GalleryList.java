package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class GalleryList {
    String photo_id, photo_title, photo_desc, photo_url;

    public GalleryList(String photo_id, String photo_title, String photo_desc, String photo_url) {
        this.photo_id = photo_id;
        this.photo_title = photo_title;
        this.photo_desc = photo_desc;
        this.photo_url = photo_url;
    }


    public String getPhoto_id() {
        return photo_id;
    }

    public String getPhoto_title() {
        return photo_title;
    }

    public String getPhoto_desc() {
        return photo_desc;
    }

    public String getPhoto_url() {
        return photo_url;
    }
}
