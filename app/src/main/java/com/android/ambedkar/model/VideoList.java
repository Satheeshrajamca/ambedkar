package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class VideoList {
    String video_id, video_title, video_image_url, video_url, video_desc;

    public VideoList(String video_id, String video_title, String video_image_url, String video_url, String video_desc) {
        this.video_id = video_id;
        this.video_title = video_title;
        this.video_image_url = video_image_url;
        this.video_url = video_url;
        this.video_desc = video_desc;
    }

    public String getVideo_id() {
        return video_id;
    }

    public String getVideo_title() {
        return video_title;
    }

    public String getVideo_image_url() {
        return video_image_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public String getVideo_desc() {
        return video_desc;
    }
}
