package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class AlbumList {
    String album_id, album_name, album_cover_photo, album_photos_count;

    public AlbumList(String album_id, String album_name, String album_cover_photo, String album_photos_count) {
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_cover_photo = album_cover_photo;
        this.album_photos_count = album_photos_count;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public String getAlbum_cover_photo() {
        return album_cover_photo;
    }

    public String getAlbum_photos_count() {
        return album_photos_count;
    }
}
