package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class BookList {
    String book_id, book_name, book_url, book_cover_img_url, book_desc;

    public BookList(String book_id, String book_name, String book_url, String book_cover_img_url, String book_desc) {
        this.book_id = book_id;
        this.book_name = book_name;
        this.book_url = book_url;
        this.book_cover_img_url = book_cover_img_url;
        this.book_desc = book_desc;
    }

    public String getBook_id() {
        return book_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public String getBook_url() {
        return book_url;
    }

    public String getBook_cover_img_url() {
        return book_cover_img_url;
    }

    public String getBook_desc() {
        return book_desc;
    }
}
