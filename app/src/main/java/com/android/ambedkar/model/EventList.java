package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class EventList {
    String event_id, event_title, event_desc, event_image_url, event_date, event_venue;

    public String getEvent_id() {
        return event_id;
    }

    public String getEvent_title() {
        return event_title;
    }

    public String getEvent_desc() {
        return event_desc;
    }

    public String getEvent_image_url() {
        return event_image_url;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getEvent_venue() {
        return event_venue;
    }

    public EventList(String event_id, String event_title, String event_desc, String event_image_url, String event_date, String event_venue) {
        this.event_id = event_id;
        this.event_title = event_title;
        this.event_desc = event_desc;
        this.event_image_url = event_image_url;
        this.event_date = event_date;
        this.event_venue = event_venue;
    }


}
