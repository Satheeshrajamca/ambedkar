package com.android.ambedkar.model;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class QuoteList {
    String quote_id, quote;

    public QuoteList(String quote_id, String quote) {
        this.quote_id = quote_id;
        this.quote = quote;
    }

    public String getQuote_id() {
        return quote_id;
    }

    public String getQuote() {
        return quote;
    }
}
