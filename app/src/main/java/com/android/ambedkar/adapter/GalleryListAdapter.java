package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.model.GalleryList;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.android.ambedkar.utility.Utill.ALBUM125PATH;
import static com.android.ambedkar.utility.Utill.ALBUMPATH;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class GalleryListAdapter extends ArrayAdapter<GalleryList> {
    Context context;
    List<GalleryList> listAlbum;
    String album_id;

    public GalleryListAdapter(@NonNull Context context, @LayoutRes int resource, List<GalleryList> listAlbum, String album_id) {
        super(context, resource, listAlbum);
        this.context = context;
        this.listAlbum = listAlbum;
        this.album_id = album_id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_gallery, parent, false);
        final GalleryList albumList = listAlbum.get(position);
        final ImageView imageGalleryThumb = (ImageView) rowView.findViewById(R.id.imageGallery);
        if (Utill.isOnline(context))
            Picasso.with(context).load(albumList.getPhoto_url()).networkPolicy(NetworkPolicy.OFFLINE).centerCrop().fit().into(imageGalleryThumb, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context)
                            .load(albumList.getPhoto_url())
                            .centerCrop().fit()
                            .into(imageGalleryThumb);
                }
            });
        else {
            if (!album_id.isEmpty()) {
                Picasso.with(context).load(new File(context.getExternalFilesDir("") + ALBUMPATH + album_id, albumList.getPhoto_id() + ".jpg")).
                        error(R.drawable.error_drawable).centerCrop().fit().into(imageGalleryThumb);
            } else {
                Picasso.with(context).load(new File(context.getExternalFilesDir("") + ALBUM125PATH, albumList.getPhoto_id() + ".jpg")).
                        error(R.drawable.error_drawable).centerCrop().fit().into(imageGalleryThumb);
            }
        }
        return rowView;
    }
}
