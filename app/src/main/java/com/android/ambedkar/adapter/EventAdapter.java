package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.model.EventList;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.android.ambedkar.utility.Utill.EVENTPATH;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class EventAdapter extends ArrayAdapter<EventList> {
    Context context;
    List<EventList> listEvents;

    public EventAdapter(@NonNull Context context, @LayoutRes int resource, List<EventList> listEvents) {
        super(context, resource, listEvents);
        this.context = context;
        this.listEvents = listEvents;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_events, parent, false);
        EventList eventList = listEvents.get(position);
        ImageView imageGalleryThumb = (ImageView) rowView.findViewById(R.id.imageEventThumb);
        TextView textEventHeading = (TextView) rowView.findViewById(R.id.textEventHeading);
        TextView textEventDesc = (TextView) rowView.findViewById(R.id.textEventDesc);
        TextView textEventDate = (TextView) rowView.findViewById(R.id.textEventDate);
        TextView textEventLocation = (TextView) rowView.findViewById(R.id.textEventLocation);
        if (Utill.isOnline(context)) {
            Picasso.with(context).load(eventList.getEvent_image_url()).centerCrop().fit().into(imageGalleryThumb);
        } else {
            Picasso.with(context).load(new File(context.getExternalFilesDir("") + EVENTPATH + eventList.getEvent_id() + ".jpg")).
                    error(R.drawable.error_drawable).centerCrop().fit().into(imageGalleryThumb);
        }
        textEventHeading.setText(eventList.getEvent_title());
        textEventDesc.setText(eventList.getEvent_desc());
        textEventDate.setText(eventList.getEvent_date());
        textEventLocation.setText(eventList.getEvent_venue());

        return rowView;
    }
}
