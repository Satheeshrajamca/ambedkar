package com.android.ambedkar.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.fragment.AnniversaryVideoFragment;
import com.android.ambedkar.fragment.VideosFragment;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.model.VideoList;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.android.ambedkar.utility.Utill.ALBUMPATH;
import static com.android.ambedkar.utility.Utill.VIDEOPATH;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class VideoListAdapter extends ArrayAdapter<VideoList> {

    Context context;
    List<VideoList> listVideos;
    String videoPath;
    VideosFragment videosFragment;
    AnniversaryVideoFragment anniversaryVideoFragment;

    public interface VideoDescription {
        public void showVideoDescription(int position);
    }

    public VideoListAdapter(VideosFragment videosFragment, Context context, int row_gallery, List<VideoList> listVideos, String videoPath) {
        super(context, row_gallery, listVideos);
        this.context = context;
        this.listVideos = listVideos;
        this.videoPath = videoPath;
        this.videosFragment = videosFragment;
    }

    public VideoListAdapter(AnniversaryVideoFragment anniversaryVideoFragment, Context context, int row_gallery, List<VideoList> listVideos, String videoPath) {
        super(context, row_gallery, listVideos);
        this.context = context;
        this.listVideos = listVideos;
        this.videoPath = videoPath;
        this.anniversaryVideoFragment = anniversaryVideoFragment;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_video, parent, false);
        VideoList videoList = listVideos.get(position);
        ImageView imageGalleryThumb = (ImageView) rowView.findViewById(R.id.imageGallery);
        ImageView imageVideoDes = (ImageView) rowView.findViewById(R.id.videoInfo);
        if (Utill.isOnline(context)) {
            Picasso.with(context).load(videoList.getVideo_image_url()).centerCrop().fit().into(imageGalleryThumb);
        } else {
            Picasso.with(context).load(new File(context.getExternalFilesDir("") + videoPath + videoList.getVideo_id() + ".jpg")).
                    error(R.drawable.error_drawable).centerCrop().fit().into(imageGalleryThumb);
        }
        imageVideoDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videosFragment != null)
                    videosFragment.showVideoDescription(position);
                else
                    anniversaryVideoFragment.showVideoDescription(position);
            }
        });

        return rowView;
    }
}
