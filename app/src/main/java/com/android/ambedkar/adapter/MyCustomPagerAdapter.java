package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.ambedkar.R;
import com.android.ambedkar.model.GalleryList;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static com.android.ambedkar.utility.Utill.ALBUM125PATH;
import static com.android.ambedkar.utility.Utill.ALBUMPATH;

/**
 * Created by satheeshraja on 4/25/17.
 */

public class MyCustomPagerAdapter extends PagerAdapter {
    Context context;
    List<GalleryList> listGallery;
    LayoutInflater layoutInflater;
    private String albumId;


    public MyCustomPagerAdapter(Context context, List<GalleryList> listGallery, String albumId) {
        this.context = context;
        this.listGallery = listGallery;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.albumId = albumId;
    }


    @Override
    public int getCount() {
        Log.i("GallerySize",""+listGallery.size());
        return listGallery.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.row_swipe_gallery, container, false);
        Log.i("GalleryList", new File(context.getExternalFilesDir("") + ALBUMPATH + albumId, listGallery.get(position).getPhoto_id()) + ".jpg");
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.imageSwipeGallery);
        if (Utill.isOnline(context)) {
            Picasso.with(context).load(listGallery.get(position).getPhoto_url()).networkPolicy(NetworkPolicy.OFFLINE).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context).load(listGallery.get(position).getPhoto_url()).into(imageView);
                }
            });
        } else {
            if (!albumId.isEmpty()) {
                Picasso.with(context).load(new File(context.getExternalFilesDir("") + ALBUMPATH + albumId, listGallery.get(position).getPhoto_id() + ".jpg")).
                        error(R.drawable.error_drawable).into(imageView);
            } else {
                Picasso.with(context).load(new File(context.getExternalFilesDir("") + ALBUM125PATH, listGallery.get(position).getPhoto_id() + ".jpg")).
                        error(R.drawable.error_drawable).into(imageView);
            }

        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
