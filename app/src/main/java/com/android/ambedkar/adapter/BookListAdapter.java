package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.model.BookList;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class BookListAdapter extends ArrayAdapter<BookList> {
    Context context;
    List<BookList> listBooks;

    public BookListAdapter(@NonNull Context context, @LayoutRes int resource, List<BookList> listBooks) {
        super(context, resource, listBooks);
        this.context = context;
        this.listBooks = listBooks;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_books, parent, false);
        BookList bookList = listBooks.get(position);
        ImageView imageGalleryThumb = (ImageView) rowView.findViewById(R.id.imageGallery);
        TextView textBookTitle = (TextView) rowView.findViewById(R.id.bookTitle);
        Picasso.with(context).load(R.drawable.book_cover).fit().into(imageGalleryThumb);
        textBookTitle.setText(bookList.getBook_name());

        return rowView;
    }
}
