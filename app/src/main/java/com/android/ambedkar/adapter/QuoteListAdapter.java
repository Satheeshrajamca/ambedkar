package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.model.EventList;
import com.android.ambedkar.model.QuoteList;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class QuoteListAdapter extends ArrayAdapter<QuoteList> {
    Context context;
    List<QuoteList> listQuotes;

    public QuoteListAdapter(@NonNull Context context, @LayoutRes int resource, List<QuoteList> listQuotes) {
        super(context, resource, listQuotes);
        this.context = context;
        this.listQuotes = listQuotes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_quotes, parent, false);
        QuoteList quotesList = listQuotes.get(position);
        TextView textQuotes = (TextView) rowView.findViewById(R.id.textQuotes);
        textQuotes.setText(quotesList.getQuote());
        return rowView;
    }
}
