package com.android.ambedkar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ambedkar.R;
import com.android.ambedkar.model.AlbumList;
import com.android.ambedkar.utility.Utill;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by satheeshraja on 4/24/17.
 */

public class AlbumListAdapter extends ArrayAdapter<AlbumList> {
    Context context;
    List<AlbumList> listAlbum;

    public AlbumListAdapter(@NonNull Context context, @LayoutRes int resource, List<AlbumList> listAlbum) {
        super(context, resource, listAlbum);
        this.context = context;
        this.listAlbum = listAlbum;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_album, parent, false);
        final AlbumList albumList = listAlbum.get(position);
        final ImageView imageGalleryThumb = (ImageView) rowView.findViewById(R.id.imageGallery);
        TextView textGalleryNo = (TextView) rowView.findViewById(R.id.galleryNo);
        TextView textNoofPictures = (TextView) rowView.findViewById(R.id.noofPictures);
        if (Utill.isOnline(context))
            Picasso.with(context).load(albumList.getAlbum_cover_photo()).networkPolicy(NetworkPolicy.OFFLINE).centerCrop().fit().into(imageGalleryThumb, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context).load(albumList.getAlbum_cover_photo()).error(R.drawable.error_drawable).centerCrop().fit().into(imageGalleryThumb);
                }
            });
        else
            Picasso.with(context).load(new File(context.getExternalFilesDir("")+"/Album/", albumList.getAlbum_id() + ".jpg")).centerCrop().fit().into(imageGalleryThumb);
        textGalleryNo.setText(context.getString(R.string.msg_gallery_no) + (position + 1));
        textNoofPictures.setText(albumList.getAlbum_photos_count() + context.getString(R.string.msg_no_pictures));

        return rowView;
    }
}
