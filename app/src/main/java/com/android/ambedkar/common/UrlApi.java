package com.android.ambedkar.common;

/**
 * Created by satheeshraja on 4/23/17.
 */

public class UrlApi {
    public static String urlGettingAlbum ="http://www.beebasoft.com/ambedkar/api/album.php";
    public static String urlGettingVideo="http://www.beebasoft.com/ambedkar/api/video.php";
    public static String urlGettingGallery ="http://www.beebasoft.com/ambedkar/api/photos.php";
    public static String urlGettingBooks ="http://www.beebasoft.com/ambedkar/api/books.php";
    public static String urlEvents ="http://www.beebasoft.com/ambedkar/api/events.php";
    public static String urlQuotes ="http://www.beebasoft.com/ambedkar/api/quotes.php";
    public static String urlGettingGallery125Celebration ="http://www.beebasoft.com/ambedkar/api/photos_125.php";
}
